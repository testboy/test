## API Specifications (HW7)
---
These are the specifications to our API. Examples are listed for all endpoints. Usage with curl and the request libarary are also included near the end of the doc.

---
## Endpoints
These are the possible endpoints to our API. They would all be extensions to address of our server, "~".  
For example "~" may be http://172.17.0.1:5000 or some other address, perhaps http://localhost:4330 or any other random example.

---
### GET ~/spots?start=<start\>&end=<end\>&limit=<limit\>&offset=<offset\>
The spots endpoint has four possible parameters that all except int values.  
You may use the start and end parameters concurrently.  
You may also use the limit and offset parameters concurrently.  
However start or end may not be used along side limit or offset.

##### No parameters
	> get ~/spots
	[
    	{
        	"id": 0,
            "spots": 101,
            "year": 1770
        },
        ...
        {
        	"id": 99,
            "spots": 74,
            "year": 1869
        }
	]
Returns a JSON list of all datapoints in the dataset.

##### Start parameter
	> get ~/spots?start=1822
	[
    	{
        	"id": 52,
            "spots": 4,
            "year": 1822
        },
        ...
        {
        	"id": 99,
            "spots": 74,
            "year": 1869
        }
	]
Returns a JSON list of every datapoint with year 1822 and beyond.

##### End parameter
	> get ~/spots?end=1840
	[
    	{
        	"id": 0,
            "spots": 101,
            "year": 1770
        },
        ...
        {
        	"id": 70,
            "spots": 65,
            "year": 1840
        }
	]
Returns a JSON list of every datapoint up to and including the year 1840.

##### Limit parameter
	> get ~/spots?limit=17
	[
    	{
        	"id": 0,
            "spots": 101,
            "year": 1770
        },
        ...
        {
        	"id": 16,
            "spots": 83,
            "year": 1786
        }
	]
Returns a JSON list of 17 datapoints starting from the first datapoint in the dataset.

##### Offset parameter
	> get ~/spots?offset=78
    [
    	{
        	"id": 78,
            "spots": 125,
            "year": 1848
        },
        ...
        {
        	"id": 99,
            "spots": 74,
            "year": 1869
        }
	]
Returns a JSON list of every datapoint after datapoint 78 in the dataset.

##### Parameter combinations 
	> get ~/spots?start=1860&end=1866
	[
    	{
        	"id": 90,
            "spots": 125,
            "year": 1860
        },
        ...
        {
        	"id": 96,
            "spots": 16,
            "year": 1866
        }
	]
Returns a JSON list of every datapoint with year between and including 1860 to 1866.

	> get ~/spots?limit=19&offset=4
	[
    	{
        	"id": 4,
            "spots": 31,
            "year": 1774
        },
        ...
        {
        	"id": 22,
            "spots": 60,
            "year": 1792
        }
	]
Returns a JSON list of 19 datapoints starting from and including datapoint 4.

	> get ~/spots?start=1920&limit=2
	"Start and end can only be used independently from limit and offset."
Returns a JSON string with an error message as you may only use start or end independently of limit or offset.

---
### POST ~/spots
	> post ~/spots
	<Response [202]>
Must post JSON dict as payload following correct syntax.
Returns JSON response of 200 and adds your inputted JSON dict to the dataset.

---
### GET ~/spots/years/<year\>
	> get ~/spots/years/1790
	{
    	"id": 20,
      	"spots": 90,
     	"year": 1790
    }
Returns the JSON dictionary of information pertaining to the inputted year.

---
### GET ~/spots/ids/<id>
	> get ~/spots/ids/23
	{
    	"id": 17,
      	"spots": 132,
     	"year": 1787
    }
Returns the JSON dictionary of information pertaining to the inputted id.

---
### GET ~/spots/graphs
	> get ~/spots/graphs
	<Response [200]>
Returns a graph of the dataset with a JSON response of code 200.

---
### GET ~/spots/stats
	> get ~/spots/
	{
    	"mean spots": 203,
      	"median spots": 232,
    }
Returns a JSON dict of all stat options.

---
### GET ~/spots/stats/mean
	> get ~/spots/stats/mean
	203
Return a JSON int with the mean of the dataset.

---
### GET ~/spots/stats/median
	> get ~/spots/stats/median
	232
Return a JSON int with the median of the dataset.

---
### GET ~/jobs
	> get ~/jobs
	[
    	{
        	"id": uuid1,
            "status": "Accepted"
        },
        ...
        {
        	"id": uuid2,
            "year": "Processing
        }
	]
Returns a JSON list of all jobs id's and their statuses.

---
### POST ~/jobs
	> post ~/jobs
	{
    	"id": uuid3,
        "status": Accepted
    }
Must post with payload related to your job.
Returns the uuid tied to your job, it's status, and adds your job to the queue of jobs.

---
### GET ~/jobs/<uuid\>
	> get ~/jobs/<uuid\>
	{
    	"id": uuid3,
		"job description": "Doing ... job on ... data"
        "status": Completed
    }
Returns a JSON dict corresponding with the uuid given with job description and status.

---
## Examples

---
### Curl
	$ curl "http:/localhost:5000/spots?start=1820&end=1821"
    [
    	{
        	"id": 50,
            "spots": 16,
            "year": 1820
        },
        {
        	"id": 51,
            "spots": 7,
            "year": 1821
        }
	]
    
---
### Requests with Python
	$ ipython3
    [1] import requests as r
    [2] res = r.get("http:/localhost:5000/spots?start=1820&end=1821")
    [3] res
    <Response [200]>
    [4] res.json()
    [{"id": 50,"spots": 16,"year": 1820},{"id": 51,"spots": 7,"year": 1821}]

---
## Charge Structure
For a basic data-oriented API like this we could allow 8 gets to the server per day. Post and more gets would come with a one-time fee. This means that our data can be used by many new people and they could see the worth of the data and API. Students and other third-parties would be able to raise publicity with their use of the API. For users that need added functionality or just more use, the user can pay the one-time fee so the users feels compelled to continue using the product, again raising publicity. To enforce these usage limits any access to the API would require a login from a setup account. Post would be limited only due to the fact that users in most cases wouldn't have any need in accessing any jobs or adding data. If they do need this access then payment would be neccessary as a way to raise funds for server cost.